# Premiers pas

Pour utiliser NextCloud sur un navigateur web, entrez directement l’url de l'espace que vous souhaitez rejoindre dans la barre d'adresse de votre navigateur. En général, elle se présente sous la forme <https://nextcloud.votre-collectif.fr>.

Selon l'espace que vous rejoignez, soit un identifiant et un mot de passe vous ont été communiqués : entrez-les dans les champs correspondants. Soit vous devez créer un compte vous-même : suivez les étapes indiquées par votre fournisseur de service.

> ⚠️ Si vous êtes sur un ordinateur public ou qui n’est pas le vôtre, assurez-vous de ne pas sauvegarder vos mots de passe dans le navigateur web. Si vous êtes sur votre ordinateur habituel, vous pouvez demander à votre navigateur ou à votre gestionnaire de mots de passe de s'en souvenir.

---

➡️ Continuer en [découvrant la page d'accueil](<1.1. Découvrir la page d'accueil.md>).
