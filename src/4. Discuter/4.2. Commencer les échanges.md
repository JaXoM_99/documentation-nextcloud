# Commencer les échanges

## Discuter

Pour commencer à discuter :

-   Écrivez votre message dans le champ en bas de la vue principale et appuyez sur
    la touche entrée du clavier.
* Vous pouvez ajouter des émojis en cliquant à gauche du champ d'écriture.

  ![](ecrire-message.png)
* Vous pouvez **répondre à un message** en particulier en cliquant sur la flèche qui apparaît à droite au survol de ce message.

   <video width="640" height="480" controls>
      <source src="repondre-message.mp4" type="video/mp4">
    </video>
* Dans le menu d'action du message, vous pouvez choisir de **répondre en privé** à l'expéditeur⋅ice. Cette action démarre une discussion privée.

  ![](repondre-prive.png)

## Lancer un appel vidéo

> ⚠️ Cette option fonctionne bien en discussion privée (deux personnes). Si vous êtes un groupe et que vous rencontrez des difficultés, nous vous recommandons plutôt d'utiliser un outil dédié à la visioconférence comme [Big Blue Button](https://bigbluebutton.org/) ou [Jitsi](https://meet.jit.si/).

Quand vous êtes dans une discussion, vous pouvez débuter un **appel vidéo** en cliquant sur "Commencer l'appel". Une notification est envoyée à votre interlocuteur⋅ice. Si c'est vous qui rejoignez un appel déjà commencé, le bouton apparaît en vert et affiche "Rejoindre l'appel".

![](commencer-appel.png)

Pendant un appel, vous pouvez :

1. Couper votre micro
2. Couper votre caméra
3.  Partager votre écran
4.  Cacher votre vidéo (pour vous)

<figure>
<img src="appel.png" alt="Interface d'un appel vidéo" />
<figcaption><i>La vue principale affiche la vidéo de votre correspondant⋅e. Un aperçu de votre vidéo est en bas à droite. Vous pouvez le cacher en cliquant sur la flèche descendante au-dessus de cet aperçu. Les icônes du micro, de la caméra et du partage d'écran sont en bas de l'aperçu. </i></figcaption>
</figure>

---

➡️ Continuer en [créant une discussion spécifiquement pour un document](<4.3. Discuter autour d'un document.md>).
