# Nextcloud sur mobile

Nextcloud permet la synchronisation de plusieurs de ses modules sur mobile (Android et iOS) :

* Fichiers
* Discussion
* Contacts et Agenda

Pour chaque module, une application différente est nécessaire.

---
➡️ Commencer par [gérer les fichiers sur mobile](<../8. Nextcloud sur mobile/8.1. Gérer ses fichiers sur mobile.md>).