# Déposer un document

-   Cliquez sur le bouton **+** en haut de la vue principale pour ouvrir le menu.

-   Choisissez "Envoyer un fichier" dans le menu déroulant.

    ![](envoyer-fichier.png)

-   La fenêtre "Ouvrir un fichier" de votre navigateur apparaît et vous
    pouvez ainsi parcourir vos données locales (sur votre ordinateur).
    Sélectionnez le ou les fichiers (Maintenez la touche Ctrl enfoncée
    pour sélectionner plusieurs fichiers) puis validez.

-   Patientez pendant l'envoi.

<video width="640" height="480" controls>
  <source src="deposer-fichiers.mp4" type="video/mp4">
</video>

Déposer par cliquer-glisser
---------------------------
<video width="640" height="480" controls>
  <source src="deposer-fichier-cliquer-glisser.mp4" type="video/mp4">
</video>


---

➡️ Continuer en [déplaçant un document](<2.3. Déplacer des documents.md>).
